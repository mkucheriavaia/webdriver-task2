package com.epam.rd.aqa.webdriver.task2;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PastebinComTest {

    public WebDriver driver;

    @BeforeAll
    public void createNewPaste() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        PastebinComPage.doPreCondition(driver);
    }

    @AfterAll
    public void cleanUp() {
        driver.quit();
    }

    @Test
    public void testTitle() {
        assertEquals(PastebinComPage.NAME, PastebinComPage.getTitleText(driver));
    }

    @Test
    public void testSyntaxHighlighting() {
        assertEquals("Bash", PastebinComPage.getSyntaxHighlightingText(driver));
    }

    @Test
    public void testCode() {
        assertEquals(PastebinComPage.CODE, PastebinComPage.getCodeText(driver));
    }
}