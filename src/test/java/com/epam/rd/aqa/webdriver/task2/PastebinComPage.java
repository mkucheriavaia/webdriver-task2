package com.epam.rd.aqa.webdriver.task2;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class PastebinComPage {
    public static final String HTTPS_PASTEBIN_COM = "https://pastebin.com/";
    public static final By COOKIE_BANNER_AGREE_BTN = By.xpath("//button[@mode='primary']");
    public static final String HIDE_AD_SCRIPT = "arguments[0].style.display='none'";
    public static final By AD_LOCATOR = By.xpath("//div[@id='vi-smartbanner']");
    public static final By SYNTAX_HIGHLIGHTING_ON = By.className("toggle__control");
    public static final By CODE_TEXTAREA = By.id("postform-text");
    public static final By PASTE_EXPIRATION_DROPDOWN = By.id("select2-postform-expiration-container");
    public static final By TEN_MINUTES_PASTE_EXPIRATION = By.xpath("//li[text()='10 Minutes']");
    public static final By PASTE_NAME_INPUT_FIELD = By.id("postform-name");
    public static final By CREATE_NEW_PASTE_BTN = By.cssSelector("button.btn");
    public static final String CODE = """
            git config --global user.name  "New Sheriff in Town"
            git reset $(git commit-tree HEAD^{tree} -m "Legacy code")
            git push origin master --force""";
    public static final String NAME = "how to gain dominance among developers";
    public static final By SYNTAX_HIGHLIGHTING = By.xpath("//div[@class='highlighted-code']/div[@class='top-buttons']/div[@class='left']/a[1]");
    public static final By TITLE = By.cssSelector("div.info-top");
    public static final By SAVED_CODE = By.xpath("//div/ol");
    public static final By SYNTAX_HIGHLIGHTING_DROPDOWN = By.id("select2-postform-format-container");
    public static final By BASH_SYNTAX_HIGHLIGHTING = By.xpath("//li[text()='Bash']");

    public static void doPreCondition(WebDriver driver) {
        openPastebinComHomePage(driver);

        insertCode(driver);

        setSyntaxHighlightingOn(driver);

        setSyntaxHighlightingBash(driver);

        setPasteExpirationToTenMinutes(driver);

        setPasteName(driver);

        clickOnCreateNewPaste(driver);
    }

    private static void openPastebinComHomePage(WebDriver driver) {
        driver.get(HTTPS_PASTEBIN_COM);
        Optional.ofNullable(
                        new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                                .until(ExpectedConditions.elementToBeClickable(COOKIE_BANNER_AGREE_BTN)))
                .ifPresent(WebElement::click);
        Optional.ofNullable(
                        new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                                .until(ExpectedConditions.presenceOfElementLocated(AD_LOCATOR)))
                .ifPresent(ad -> ((JavascriptExecutor) driver).executeScript(HIDE_AD_SCRIPT, ad));
    }

    private static void setSyntaxHighlightingOn(WebDriver driver) {
        driver.findElement(SYNTAX_HIGHLIGHTING_ON).click();
    }

    private static void insertCode(WebDriver driver) {
        WebElement textArea = driver.findElement(CODE_TEXTAREA);
        textArea.sendKeys(CODE);
    }

    private static void setSyntaxHighlightingBash(WebDriver driver) {
        WebElement syntaxHighlighting = driver.findElement(SYNTAX_HIGHLIGHTING_DROPDOWN);
        syntaxHighlighting.click();
        driver.findElement(BASH_SYNTAX_HIGHLIGHTING).click();
    }

    private static void setPasteExpirationToTenMinutes(WebDriver driver) {
        WebElement pastExpiration = driver.findElement(PASTE_EXPIRATION_DROPDOWN);
        pastExpiration.click();
        driver.findElement(TEN_MINUTES_PASTE_EXPIRATION).click();
    }

    private static void setPasteName(WebDriver driver) {
        WebElement pasteName = driver.findElement(PASTE_NAME_INPUT_FIELD);
        pasteName.sendKeys(NAME);
    }

    private static void clickOnCreateNewPaste(WebDriver driver) {
        WebElement createNewPasteBtn = driver.findElement(CREATE_NEW_PASTE_BTN);
        createNewPasteBtn.click();
    }

    public static String getTitleText(WebDriver driver) {
        return driver.findElement(TITLE).getText();
    }

    public static String getSyntaxHighlightingText(WebDriver driver) {
        return driver.findElement(SYNTAX_HIGHLIGHTING).getText();
    }

    public static String getCodeText(WebDriver driver) {
        return driver.findElement(SAVED_CODE).getText();
    }
}
